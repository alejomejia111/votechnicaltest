﻿///*********
///
/// 
/// El coraje es ir de fracaso en fracaso sin pérdida de entusiasmo (Winston S. Churchill)
/// 
/// 
/// Elaborado por: Alejandro Mejía Morales
///*********


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CSNamedPipe;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Timers;
using Microsoft.Win32;

namespace TechnicalTestAdministrator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NamedPipeServer PServer1 = new NamedPipeServer(@"\\.\pipe\myNamedPipe3", 0);
        NamedPipeServer PServer2 = new NamedPipeServer(@"\\.\pipe\myNamedPipe4", 1);
            
        ProcessStartInfo startInfo = new ProcessStartInfo();
        Process exeProcess = new Process();

        System.Timers.Timer timer1 = new System.Timers.Timer();

        private IntPtr hWndOriginalParent;
        private IntPtr hWndDocked;
        private IntPtr hWndStupidWindow;

        [DllImport("user32.dll")]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet=CharSet.Unicode)]
        static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        const int GWL_EXSTYLE = -20;
        const int WS_EX_DLGMODALFRAME = 0x0001;
        const int SWP_NOSIZE = 0x0001;
        const int SWP_NOMOVE = 0x0002;
        const int SWP_NOZORDER = 0x0004;
        const int SWP_FRAMECHANGED = 0x0020;
        const uint WM_SETICON = 0x0080;

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter, int x, int y, int width, int height, uint flags);

        OpenFileDialog openFileDialog = new OpenFileDialog();


        public MainWindow()
        {
            InitializeComponent();

            PServer1.Start();
            PServer2.Start();

            timer1.Interval = 1;
            timer1.Elapsed += Timer1Elapsed;
            timer1.Start();

            openFileDialog.FileOk += OpenFileDialog_FileOk;
        }

        private void Timer1Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                if(PServer1.readValue != "")
                {
                    if(PServer1.readValue.Contains("$InfoLog$"))
                    {
                        PServer1.readValue = PServer1.readValue.Remove(0, 9);
                        TextBlockInformation.Text = " >> " + PServer1.readValue + Environment.NewLine + TextBlockInformation.Text; 
                    }
                    else if(PServer1.readValue.Contains("$Data$"))
                    {
                        PServer1.readValue = PServer1.readValue.Remove(0, 6);
                        TextBlockMeasurements.Text = TextBlockMeasurements.Text + Environment.NewLine + " |--> " + PServer1.readValue;
                    }
                    PServer1.readValue = "";
                }
            });
        }

        private void Button_BinaryExecuting_Click(object sender, RoutedEventArgs e)
        {            
            String Message = "hallo";
            PServer2.SendMessage(Message, PServer2.clientse);

            TextBlockMeasurements.Text = "";
        }
        
        private void ClosedApp(object sender, EventArgs e)
        {
        }

        private void ClosingWindow(object sender, System.ComponentModel.CancelEventArgs e)
        {
            exeProcess.Kill();

            PServer1.StopServer();
            PServer2.StopServer();

            timer1.Stop();
        }

        private void LoadedWindow(object sender, RoutedEventArgs e)
        {
        }

        private void ContentRenderedWindow(object sender, EventArgs e)
        {
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "TechnicalTestKernel.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        
            exeProcess.StartInfo = startInfo;
            exeProcess.Start();

            Thread.Sleep(1500);

            for(int i=0; i<=10; i++)
            {
                
                hWndDocked = exeProcess.MainWindowHandle;
                SetWindowLong(hWndDocked, GWL_STYLE, GetWindowLong(hWndDocked, GWL_STYLE) & ~WS_SYSMENU);
                SetWindowPos(hWndDocked, IntPtr.Zero, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE );

                hWndOriginalParent = SetParent(hWndDocked, new WindowInteropHelper(this).Handle);

                hWndStupidWindow = FindWindowEx(hWndDocked, IntPtr.Zero, null, "PCLViewer");
                SetWindowLong(hWndStupidWindow, GWL_STYLE, GetWindowLong(hWndStupidWindow, GWL_STYLE) & ~WS_SYSMENU);
                SetWindowPos(hWndStupidWindow, IntPtr.Zero, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE );

                hWndOriginalParent = SetParent(hWndStupidWindow, new WindowInteropHelper(this).Handle);
                Thread.Sleep(100);
            }
        }

        private void ButtonPointCloudPath_Click(object sender, RoutedEventArgs e)
        {
            openFileDialog.Filter = "Point Cloud File (*.ply)|*.ply";
            openFileDialog.ShowDialog();
        }

        private void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            String filePath = openFileDialog.FileName;
            PServer2.SendMessage(filePath, PServer2.clientse);
        }
    }
}
