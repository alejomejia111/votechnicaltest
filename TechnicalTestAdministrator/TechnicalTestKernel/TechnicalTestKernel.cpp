﻿// TechnicalTestKernel.cpp : Defines the entry point for the application.
//*********
//
// 
// El coraje es ir de fracaso en fracaso sin pérdida de entusiasmo (Winston S. Churchill)
// 
// 
// Elaborado por: Alejandro Mejía Morales
//*********


#include "TechnicalTestKernel.h"

#include <stdio.h>
#include <iostream>
#include <thread>
#include <future>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/window.h>
#include <pcl/visualization/vtk/vtkRenderWindowInteractorFix.h>
#include <pcl/visualization/vtk.h>


#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/median_filter.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/console/parse.h>

#include <pcl/sample_consensus/sac_model_circle3d.h>
#include <pcl/sample_consensus/sac_model_cylinder.h>
#include <pcl/sample_consensus/ransac.h>

#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/conditional_removal.h>

#include <pcl/features/boundary.h>
#include <pcl/features/feature.h>
#include <pcl/segmentation/supervoxel_clustering.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/segmentation/conditional_euclidean_clustering.h>

#include <boost/thread/thread.hpp>

#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>

#include <pcl/segmentation/extract_clusters.h>

using namespace std;


void Updater();
void Listener();
void CodeExecuter();
void Writer(string valueToSend);

HANDLE hPipe1, hPipe2;

bool updaterMakeChoice = false;
bool toRemovePointClouds = false;
bool toAddPointClouds = false;

float redIntensity_toAddCloud = 1.0;
float greenIntensity_toAddCloud = 1.0;
float blueIntensity_toAddCloud = 1.0;

pcl::PointCloud<pcl::PointXYZ>::Ptr toAdd_PointCloud(new pcl::PointCloud<pcl::PointXYZ>);

string filePath = "C:/Schraubplatte_01.ply";


int main(int argc, char *argv[])
{
	//Pipe Init Data
	char buf[100];

	LPTSTR lpszPipename1 = TEXT("\\\\.\\pipe\\myNamedPipe3");
	LPTSTR lpszPipename2 = TEXT("\\\\.\\pipe\\myNamedPipe4");

	DWORD cbWritten;
	DWORD dwBytesToWrite = (DWORD)strlen(buf);

	//Thread Init Data
	BOOL Write_St = TRUE;
	   	  
	hPipe1 = CreateFile(lpszPipename1, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
	hPipe2 = CreateFile(lpszPipename2, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);


	if ((hPipe1 == NULL || hPipe1 == INVALID_HANDLE_VALUE) || (hPipe2 == NULL || hPipe2 == INVALID_HANDLE_VALUE))
	{
		printf("Could not open the pipe  - (error %d)\n", GetLastError());
		Writer("Could not open the communication");
	}
	else
	{
		std::thread First(Listener);
		std::thread Second(Updater);

		First.join();
		Second.join();
	}

	getchar();
}


void Updater() {
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerOriginal(new pcl::visualization::PCLVisualizer("PCLViewer", true));
	
	HWND hWnd = FindWindow("MainWindow", NULL);

	viewerOriginal->getRenderWindow()->BordersOff();
	viewerOriginal->getRenderWindow()->SetParentId(hWnd);
	viewerOriginal->getRenderWindow()->SetWindowName("PCLViewer");

	
	int i = 0;

	while (true)
	{
		if (!toAddPointClouds && toRemovePointClouds && updaterMakeChoice) {
			viewerOriginal->removeAllPointClouds();
			i = 0;

			viewerOriginal->resetCamera();
			toRemovePointClouds = false;
			updaterMakeChoice = false;
		}

		if (toAddPointClouds && !toRemovePointClouds && updaterMakeChoice) {
			viewerOriginal->addPointCloud(toAdd_PointCloud, std::to_string(i));
			viewerOriginal->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, redIntensity_toAddCloud, greenIntensity_toAddCloud, blueIntensity_toAddCloud, std::to_string(i));

			viewerOriginal->resetCamera();
			i++;

			toAddPointClouds = false;
			updaterMakeChoice = false;
		}

		if (toAddPointClouds && toRemovePointClouds && updaterMakeChoice) {
			viewerOriginal->removeAllPointClouds();
			i = 0;
			toRemovePointClouds = false;

			viewerOriginal->addPointCloud(toAdd_PointCloud, std::to_string(i));
			viewerOriginal->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, redIntensity_toAddCloud, greenIntensity_toAddCloud, blueIntensity_toAddCloud, std::to_string(i));
			viewerOriginal->resetCamera();
			i++;

			toAddPointClouds = false;
			updaterMakeChoice = false;
		}

		viewerOriginal->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
}


void Listener() {
	BOOL fSuccess;
	char chBuf[300];
	DWORD dwBytesToWrite = (DWORD)strlen(chBuf);
	DWORD cbRead;
	int i;

	while (true)
	{
		cout << "listening" << endl;
		fSuccess = ReadFile(hPipe2, chBuf, dwBytesToWrite, &cbRead, NULL);
		if (fSuccess)
		{
			string chBuf_total = "";
			printf("C++ App: Received %d Bytes : ", cbRead);
			for (i = 0; i < cbRead; i++) {
				printf("%c", chBuf[i]);
				chBuf_total += chBuf[i];
			}
			printf("\n");

			if (chBuf_total == "hallo") {
				std::thread Third(CodeExecuter);
				Third.join();
			}
			else {
				filePath = chBuf_total;
			}
		}
		if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
		{
			printf("Can't Read\n");
			Writer("Can't Read");
			if (true)
				break;
		}
	}
}


void Writer(string valueToSend) {
	char buf[300];

	for (int i = 0; i < valueToSend.length(); i++)
		buf[i] = valueToSend[i];

	DWORD cbWritten;
	DWORD dwBytesToWrite = (DWORD)strlen(buf);

	WriteFile(hPipe1, buf, dwBytesToWrite, &cbWritten, NULL);
	memset(buf, 0xCC, 300);
}


void CodeExecuter() {
	bool isPointCloudFound = false;

	/////////////////// READING
	pcl::PointCloud<pcl::PointXYZ>::Ptr rawCloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr semiFilteredcloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr superFilteredcloud(new pcl::PointCloud<pcl::PointXYZ>);

	Writer("$InfoLog$Reading PointCloud File...");
	if (pcl::io::loadPLYFile<pcl::PointXYZ>(filePath, *rawCloud) == -1)
	{
		PCL_ERROR("PLY File was unable to load \n");
		Writer("$InfoLog$PLY File was unable to load");
	}
	else
	{
		cout << "PLY File loaded " << endl;
		Writer("$InfoLog$PLY File loaded ");
		isPointCloudFound = true;
	}

	if (isPointCloudFound) {
		toAdd_PointCloud = rawCloud;

		redIntensity_toAddCloud = 1.0;
		greenIntensity_toAddCloud = 1.0;
		blueIntensity_toAddCloud = 1.0;

		toAddPointClouds = true;
		updaterMakeChoice = true;

		Sleep(100);
		/////////////////// FILTERING
		Writer("$InfoLog$Applying SuperVoxel Filter... ");
		cout << "RawCloud: " << rawCloud->size() << endl;

		pcl::VoxelGrid<pcl::PointXYZ> voxelFilter;
		voxelFilter.setInputCloud(rawCloud);

		voxelFilter.setLeafSize(0.5f, 0.5f, 0.5f);
		voxelFilter.filter(*semiFilteredcloud);

		voxelFilter.setLeafSize(0.8f, 0.8f, 0.8f);
		voxelFilter.filter(*superFilteredcloud);

		cout << "SemiFilteredCloud: " << semiFilteredcloud->size() << endl;
		cout << "SuperFilteredCloud: " << superFilteredcloud->size() << endl;

		Writer("$InfoLog$PointCloud File Filtered. Displaying pointCloudFiltered...");

		// Visualize as white-pointCloud
		toAdd_PointCloud = superFilteredcloud;

		redIntensity_toAddCloud = 1.0;
		greenIntensity_toAddCloud = 1.0;
		blueIntensity_toAddCloud = 1.0;

		toAddPointClouds = true;
		toRemovePointClouds = true;
		updaterMakeChoice = true;


		/////////////////// RANSAC
		Sleep(100);
		Writer("$InfoLog$Identifying mainplane. Applying RANSAC... ");
		cout << endl << "Applying RANSAC" << endl;
		std::vector<int> inliers;
		pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::Normal> seg;
		pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
		pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

		pcl::ModelCoefficients::Ptr coefficients_plane(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers_plane(new pcl::PointIndices);

		// Estimate point normals
		ne.setSearchMethod(tree);
		ne.setInputCloud(superFilteredcloud);
		ne.setKSearch(50);
		ne.compute(*cloud_normals);

		// Create the segmentation object for cylinder segmentation and set all the parameters
		seg.setOptimizeCoefficients(false);
		seg.setModelType(pcl::SACMODEL_PLANE);
		seg.setMethodType(pcl::SAC_RANSAC);
		seg.setNormalDistanceWeight(0.1);
		seg.setMaxIterations(100);
		seg.setDistanceThreshold(3);
		seg.setInputCloud(superFilteredcloud);
		seg.setInputNormals(cloud_normals);

		// Obtain the plane inliers and coefficients
		seg.segment(*inliers_plane, *coefficients_plane);

		Writer("$InfoLog$Main Plane Found. Displaying Scene without it...");
		std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

		// Getting of negative escene - plane inliers
		pcl::ExtractIndices<pcl::PointXYZ> extract;
		extract.setInputCloud(superFilteredcloud);
		extract.setIndices(inliers_plane);
		extract.setNegative(true);
		extract.filter(*superFilteredcloud);

		// Visualize as white-pointCloud
		toAdd_PointCloud = superFilteredcloud;

		redIntensity_toAddCloud = 1.0;
		greenIntensity_toAddCloud = 1.0;
		blueIntensity_toAddCloud = 1.0;

		toAddPointClouds = true;
		toRemovePointClouds = true;
		updaterMakeChoice = true;


		/////////////////// CLUSTERING
		Writer("$InfoLog$Identifying holes in scene by EuclideanClusterExtraction and again, RANSAC for each one... ");
		pcl::search::KdTree<pcl::PointXYZ>::Ptr secondTree(new pcl::search::KdTree<pcl::PointXYZ>);
		tree->setInputCloud(superFilteredcloud);

		std::vector<pcl::PointIndices> cluster_indices;
		pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
		ec.setClusterTolerance(11);
		ec.setMinClusterSize(10);
		ec.setMaxClusterSize(200);
		ec.setSearchMethod(secondTree);
		ec.setInputCloud(superFilteredcloud);
		ec.extract(cluster_indices);

		int j = 0;
		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
		{
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
			for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
				cloud_cluster->points.push_back(superFilteredcloud->points[*pit]);

			cloud_cluster->width = cloud_cluster->points.size();
			cloud_cluster->height = 1;
			cloud_cluster->is_dense = true;

			Eigen::Vector4f centroid;
			pcl::compute3DCentroid(*cloud_cluster, centroid);

			j++;

			cout << "centroid values: " << centroid << endl;

			Writer("$InfoLog$A hole was Found ");


			/////////////// RANSAC AGAIN
			std::vector<int> inliers;
			pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::Normal> seg;
			pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
			pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
			pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

			pcl::ModelCoefficients coefficients_cylinder;
			pcl::PointIndices::Ptr inliers_cylinder(new pcl::PointIndices);


			// Estimate point normals
			ne.setSearchMethod(tree);
			ne.setInputCloud(cloud_cluster);
			ne.setKSearch(50);
			ne.compute(*cloud_normals);

			// Create the segmentation object for cylinder segmentation and set all the parameters
			seg.setOptimizeCoefficients(false);
			seg.setModelType(pcl::SACMODEL_CYLINDER);
			seg.setMethodType(pcl::SAC_RANSAC);
			seg.setNormalDistanceWeight(0.1);
			seg.setMaxIterations(100);
			seg.setDistanceThreshold(3);
			seg.setInputCloud(cloud_cluster);
			seg.setInputNormals(cloud_normals);

			// Obtain the plane inliers and coefficients
			seg.segment(*inliers_cylinder, coefficients_cylinder);
			std::cerr << "Cylinder coefficients: " << coefficients_cylinder.values[3] << std::endl;
			cout << coefficients_cylinder.values[4] << std::endl;
			cout << coefficients_cylinder.values[5] << std::endl;

			Sleep(100);
			stringstream ss;
			ss << "$Data$Centroid Hole " << j << ": " << endl << "    xAxis=" << centroid[0] << endl << "    yAxis=" << centroid[1] << endl << "    zAxis=" << centroid[2] << endl << "    xAxisNormal=" << coefficients_cylinder.values[3] << endl << "    yAxisNormal=" << coefficients_cylinder.values[4] << endl << "    zAxisNormal=" << coefficients_cylinder.values[5] << endl;
			Writer(ss.str());

			// Getting of negative escene - plane inliers
			pcl::ExtractIndices<pcl::PointXYZ> extract;
			extract.setInputCloud(cloud_cluster);
			extract.setIndices(inliers_cylinder);
			extract.setNegative(false);
			extract.filter(*cloud_cluster);

			// Visualize as white-pointCloud
			toAdd_PointCloud = cloud_cluster;

			redIntensity_toAddCloud = 0.0;
			greenIntensity_toAddCloud = 1.0;
			blueIntensity_toAddCloud = 0.0;

			toAddPointClouds = true;
			updaterMakeChoice = true;

			boost::this_thread::sleep(boost::posix_time::microseconds(200000));
		}

		stringstream ss;
		ss << "$InfoLog$A total of " << j << " holes were detected ";
		Writer(ss.str());
	}
}
